<?php

/* */

function redirect($url, $params = null) {
	header("Location: $url");
}

/*  */

function rewrite() {
	// I don't think it's really possible to rewrite,
	// the difference between rewrite() and route() is that rewrite starts from and sets the beginning of the path,
	// while route() should be relative to current script.
}

/*  */

function render($scriptPath, $params = null) {
	// do output buffering
	include $_SERVER['DOCUMENT_ROOT'] . ltrim($scriptPath, '/');
}

/* parses a route if it matches, without consuming it. */

// nevermind this one

/* checks and conditionally consumes a path against a given route with * as a wildcard for one path segment, wildcards get passed to func as params. */

function route($route, $func) {
	/* The * symbol matches one segment */
	
	$remainingAr = explode('/', trim($_SERVER['REMAINING_PATH'], '/'));
	$routeAr = explode('/', trim($route, '/'));
	$isMatch = true;
	$paramAr = array();
	//echo "$route ---\n";
	//echo "remaining " . var_dump($remainingAr);
	//echo "route " . var_dump($routeAr);
	for ($i = 0; $i < count($routeAr) && $i < count($remainingAr); $i++) {

		if ($routeAr[$i] == '*') {
			$paramAr[] = $remainingAr[$i];
		}
		
		else if ($routeAr[$i] != $remainingAr[$i]) {
			$isMatch = false;
		}

	}
	//echo "params " .var_dump($paramAr);
	
	if ($isMatch) {
		$_SERVER['CURRENT_PATH'] = rtrim($_SERVER['CURRENT_PATH'], '/') . '/' . implode('/', array_slice($remainingAr, 0, $i));
		$_SERVER['REMAINING_PATH'] = implode('/', array_slice($remainingAr, $i));
		call_user_func_array($func, $paramAr);
		return true;
	}
	else {
		return false;
	}
}

/*
 On each layer that routing is done, the routes() function should be called once with an array of all the routes on that layer,
	as opposed to calling the route() function several times, because that can lead to unexcepted behavior.
*/

function route_once($routes) {
	foreach ($routes as $route => $func) {
		if (route($route, $func))
			return true;
	}
	
	return false;
}



?>